package org.tinygroup.tinyscript.springmvc.convert;

import org.springframework.http.MediaType;
import org.tinygroup.tinyscript.springmvc.MediaTypeConvertHandler;
import org.tinygroup.tinyscript.tree.impl.DataNodeUtil;
import org.tinygroup.tinyscript.tree.impl.TreeDataNode;

public class TreeDataNodeTextConvertHandler extends MediaTypeConvertHandler{

	public TreeDataNodeTextConvertHandler() {
		super(TreeDataNode.class, MediaType.TEXT_HTML);
	}

	protected String convert(Object obj, MediaType accept) {
		TreeDataNode tree = (TreeDataNode) obj;
		return DataNodeUtil.toJson(tree);
	}

	protected String getContentType() {
		return MediaType.TEXT_HTML_VALUE;
	}

}
