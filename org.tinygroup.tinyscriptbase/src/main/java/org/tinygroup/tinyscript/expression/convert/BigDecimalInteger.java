package org.tinygroup.tinyscript.expression.convert;

import java.math.BigDecimal;

import org.tinygroup.tinyscript.expression.Converter;

public class BigDecimalInteger implements Converter<BigDecimal,Integer> {

	public Integer convert(BigDecimal object) {
		return object.intValue();
	}

	public Class<?> getSourceType() {
		return BigDecimal.class;
	}

	public Class<?> getDestType() {
		return Integer.class;
	}

}
