package org.tinygroup.tinyscript.config;

public class DefaultParameterConfig implements ParameterConfig{

	private Class<?> parameterType;
	private String name;
	private boolean arrayTag = false;
	private boolean allowNull = false;
	private boolean dynamic = false;
	private String description;
	
	public DefaultParameterConfig(String name){
		this.name = name;
	}
	
	public DefaultParameterConfig(Class<?> parameterType,String name){
		this(parameterType,name,false,false,false,null);
	}
	
	public DefaultParameterConfig(Class<?> parameterType,String name,boolean arrayTag,boolean allowNull,boolean dynamic){
		this(parameterType,name,arrayTag,allowNull,dynamic,null);
	}
	
    public DefaultParameterConfig(Class<?> parameterType,String name,boolean arrayTag,boolean allowNull,boolean dynamic,String description){
		this.parameterType = parameterType;
		this.name = name;
		this.arrayTag = arrayTag;
		this.allowNull = allowNull;
		this.dynamic = dynamic;
		this.description = description;
	}
	
	public String getParameterType() {
		return parameterType==null?Object.class.getName():parameterType.getName();
	}

	public boolean isArray() {
		return arrayTag;
	}

	public boolean allowNull() {
		return allowNull;
	}

	public boolean isDynamic() {
		return dynamic;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

}
