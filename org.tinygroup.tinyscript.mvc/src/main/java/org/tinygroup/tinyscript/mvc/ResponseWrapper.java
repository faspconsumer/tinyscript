package org.tinygroup.tinyscript.mvc;


public interface ResponseWrapper<View> {

	/**
	 * 包装响应结果
	 * @param context
	 * @param value
	 * @return
	 * @throws Exception
	 */
	View wrap(ControllerContext context,Object value) throws Exception;
}
