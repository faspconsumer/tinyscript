package org.tinygroup.tinyscript.application;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.tinygroup.application.AbstractApplicationProcessor;
import org.tinygroup.beancontainer.BeanContainer;
import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.logger.LogLevel;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;
import org.tinygroup.tinyscript.mvc.ScriptController;
import org.tinygroup.tinyscript.mvc.TinyScriptMvcManager;
import org.tinygroup.xmlparser.node.XmlNode;

/**
 * 关联控制层、服务层信息，动态处理Bean
 * @author yancheng11334
 *
 */
public class TinyScriptMvcProcessor extends AbstractApplicationProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(TinyScriptMvcProcessor.class);
	
	private XmlNode applicationConfig;
	private XmlNode componentConfig;
	
	private TinyScriptMvcManager tinyScriptMvcManager;
	    
	public TinyScriptMvcManager getTinyScriptMvcManager() {
		return tinyScriptMvcManager;
	}

	public void setTinyScriptMvcManager(TinyScriptMvcManager tinyScriptMvcManager) {
		this.tinyScriptMvcManager = tinyScriptMvcManager;
	}

	public void start() {
		LOGGER.logMessage(LogLevel.INFO, "开始加载TinyScript的MVC相关配置...");
		List<ScriptController> list = tinyScriptMvcManager.getScriptControllerList();
		regisiterBean(list);
		LOGGER.logMessage(LogLevel.INFO, "加载TinyScript的MVC相关配置完毕!");
	}

	protected void regisiterBean(List<ScriptController> list){
		BeanContainer<?>  factory =  BeanContainerFactory.getBeanContainer(getClass().getClassLoader());
		AbstractApplicationContext context = (AbstractApplicationContext) factory.getBeanContainerPrototype();
		//DefaultListableBeanFactory beanFactory =  (DefaultListableBeanFactory)context.getBeanFactory();
		
		Map<String,Object> map = new HashMap<String,Object>();
		for(ScriptController scriptController:list){
			map.put(scriptController.getUrl(), scriptController);
		}
		SimpleUrlHandlerMapping handlerMapping = (SimpleUrlHandlerMapping) context.getBean("tinyScriptMvcHandlerMapping");
		handlerMapping.setUrlMap(map);
		handlerMapping.initApplicationContext();
	}
	
	public void stop() {
		LOGGER.logMessage(LogLevel.INFO, "开始卸载TinyScript的MVC相关配置...");
		LOGGER.logMessage(LogLevel.INFO, "卸载TinyScript的MVC相关配置完毕!");
	}

	public String getApplicationNodePath() {
        return "/application/tinyscript-mvc";
    }

    public String getComponentConfigPath() {
        return "/tinyscriptmvc.config.xml";
    }


    public void config(XmlNode applicationConfig, XmlNode componentConfig) {
        this.applicationConfig = applicationConfig;
        this.componentConfig = componentConfig;
    }

    public XmlNode getComponentConfig() {
        return componentConfig;
    }

    public XmlNode getApplicationConfig() {
        return applicationConfig;
    }

    public int getOrder() {
        return 0;
    }

}
